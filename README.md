# Wolverine Fire

Demo: [https://alexpswanson.gitlab.io/wolverine_fire/](https://alexpswanson.gitlab.io/wolverine_fire/)

## Description
24 hour programming project, to display US Forest Service GIS data defining wildfire lines for 2015 Wolverine Fire in Washington State. This program was used to allow those evacuated from the fire (myself included) to view fire progression up the Railroad Creek valley and toward Holden Village (marked on map).

##### Updates:
*refactored August 2019 away from Google Maps API restrictions, and utilized [Leaflet](https://leafletjs.com/) opensource JavaScript library
